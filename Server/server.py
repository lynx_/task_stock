from selenium import webdriver
from time import sleep
import datetime
import threading
import sqlite3
from bs4 import BeautifulSoup
import datetime
import dateparser
import pytz
from math import ceil
import csv
import socket
import queue
from numpy import random
import pickle

import os

NUM_THREADS = 5

mask_threads = [0 for i in range(NUM_THREADS)]

run = True



class Bot:

	def __init__(self):
		options = webdriver.ChromeOptions()
		#options.add_argument('headless')
		options.add_argument('--log-level=3')

		self.drivers = [webdriver.Chrome(options = options) for i in range(NUM_THREADS)]




	def parsing_one(self,pos,tickets,num_drivers):
		while True:
			try:
				self.drivers[num_drivers].get('https://finance.yahoo.com/quote/{}/options?p={}'.format(tickets[pos],tickets[pos]))
				break                                         
			except:
				sleep(2)
		sleep(1)
		price_strike = 0
		while True:
			try:
				price_strike = self.drivers[num_drivers].find_element_by_xpath('//span[@class="Trsdu(0.3s) Fw(b) Fz(36px) Mb(-4px) D(ib)"]').text.replace(',','')
				break
			except:
				sleep(2)

		try:
			self.drivers[num_drivers].find_element_by_xpath('//button[@name="agree"]').click()
		except:
			pass
		date_now = datetime.datetime.date(datetime.datetime.now())
		soup = BeautifulSoup(self.drivers[num_drivers].page_source,'lxml')

		date_option = str(datetime.datetime.date(dateparser.parse(soup.find_all('option')[0].get_text())))

		type_stock = 'calls'

		type_time = 'now'

		calls = soup.find('table',class_='calls W(100%) Pos(r) Bd(0) Pt(0) list-options')
		strikes = calls.find_all('td',class_='data-col2 Ta(end) Px(10px)')

		price_bid = calls.find_all('td',class_='data-col4 Ta(end) Pstart(7px)')
		price_ask = calls.find_all('td',class_='data-col5 Ta(end) Pstart(7px)')
		volumes = calls.find_all('td',class_='data-col8 Ta(end) Pstart(7px)')
		open_interest = calls.find_all('td',class_='data-col9 Ta(end) Pstart(7px)')
		db = sqlite3.connect('info.db')
		sql = db.cursor()
		while True:
			try:
				sql.execute(f"INSERT INTO prices VALUES(?,?)",(tickets[pos],price_strike.replace(',','')))
				db.commit()
				break
			except:
				sleep(2)
		for row in range(len(strikes)):
			insert_strike = float(strikes[row].next_element.get_text().replace(',',''))
			try:
				insert_bid = float(price_bid[row].get_text().replace(',',''))
			except:
				insert_bid =-1
			try:
				insert_ask = float(price_ask[row].get_text().replace(',',''))
			except:
				insert_ask = -1
			try:
				insert_volume = int(volumes[row].get_text())
			except:
				insert_volume = -1
			try:
				insert_interest = int(open_interest[row].get_text().replace(',',''))
			except:
				insert_interest = -1
			while True:
				try:
					sql.execute(f"INSERT INTO calls_now VALUES(?,?,?,?,?,?,?,?,?,?)",(tickets[pos],insert_strike,insert_bid,insert_ask,insert_volume,insert_interest,date_option,date_now,type_stock,type_time))
					db.commit()
					break
				except:
					sleep(0.5)

		
		for row in range(len(strikes)-1):
			if float(strikes[row].next_element.get_text().replace(',','')) <= float(price_strike) and float(strikes[row+1].next_element.get_text().replace(',','')) > float(price_strike):
				pos_center = row


		try:
			central_strikes = float(strikes[pos_center].get_text().replace(',',''))
		except:
			central_strikes = 0
		try:
			central_bid = float(price_bid[pos_center].get_text().replace(',',''))
		except:
			central_bid = -1
		try:
			percent_strike = central_bid/central_strikes
		except:
			percent_strike = -1
		while True:
				try:
					sql.execute(f"INSERT INTO central_strikes VALUES(?,?,?,?,?,?,?)",(tickets[pos],central_strikes,central_bid,percent_strike,date_now,'calls',str(datetime.datetime.date(datetime.datetime.now()))))
					db.commit()
					break
				except:
					sleep(0.5)


		type_stock = 'puts'

		puts = soup.find('table',class_='puts W(100%) Pos(r) list-options')

		strikes = puts.find_all('td',class_='data-col2 Ta(end) Px(10px)')
		price_bid = puts.find_all('td',class_='data-col4 Ta(end) Pstart(7px)')
		price_ask = puts.find_all('td',class_='data-col5 Ta(end) Pstart(7px)')
		volumes = puts.find_all('td',class_='data-col8 Ta(end) Pstart(7px)')
		open_interest = puts.find_all('td',class_='data-col9 Ta(end) Pstart(7px)')

		for row in range(len(strikes)):
			insert_strike = float(strikes[row].next_element.get_text().replace(',',''))
			try:
				insert_bid = float(price_bid[row].get_text().replace(',',''))
			except:
				insert_bid = -1
			try:
				insert_ask = float(price_ask[row].get_text().replace(',',''))
			except:
				insert_ask = -1
			try:
				insert_volume = int(volumes[row].get_text())
			except:
				insert_volume = -1
			try:
				insert_interest = int(open_interest[row].get_text().replace(',',''))
			except:
				insert_interest = -1
			while True:
				try:
					sql.execute(f"INSERT INTO puts_now VALUES(?,?,?,?,?,?,?,?,?,?)",(tickets[pos],insert_strike,insert_bid,insert_ask,insert_volume,insert_interest,date_option,date_now,type_stock,type_time))
					db.commit()
					break
				except:
					sleep(0.5)


		self.drivers[num_drivers].find_element_by_xpath('//select[@class="Fz(s) H(25px) Bd Bdc($seperatorColor)"]').click()
		all_options = []
		while True:
			try:
				all_options = self.drivers[num_drivers].find_elements_by_xpath('//option')
				break
			except:
				sleep(2)

		for opt in range(len(all_options)):
			if (dateparser.parse(all_options[opt].text) - datetime.datetime.now() >= datetime.timedelta(hours=2190)) and (dateparser.parse(all_options[opt].text) - datetime.datetime.now() <= datetime.timedelta(hours=8760)):
				date_future = all_options[opt].text
				all_options[opt].click()
				sleep(1)
				while True:
					try:
						soup = BeautifulSoup(self.drivers[num_drivers].page_source,'lxml')
						puts = soup.find('table',class_='puts W(100%) Pos(r) list-options')
						strikes = puts.find_all('td',class_='data-col2 Ta(end) Px(10px)')
						break
					except:
						print('Download info')
						sleep(2)
				type_stock = 'puts'
				type_time = 'future'


				price_bid = puts.find_all('td',class_='data-col4 Ta(end) Pstart(7px)')
				price_ask = puts.find_all('td',class_='data-col5 Ta(end) Pstart(7px)')
				volumes = puts.find_all('td',class_='data-col8 Ta(end) Pstart(7px)')
				open_interest = puts.find_all('td',class_='data-col9 Ta(end) Pstart(7px)')
				len_all_row = len(puts.find_all('tr'))
				pos_center = 0
				for row in range(len(strikes)-1):
					if float(strikes[row].next_element.get_text().replace(',','')) <= float(price_strike) and float(strikes[row+1].next_element.get_text().replace(',','')) > float(price_strike):
						pos_center = row
				pos_center+=1
				try:
					central_strikes = float(strikes[pos_center].get_text().replace(',',''))
				except:
					central_strikes = 0
				try:
					central_bid = float(price_ask[pos_center].get_text().replace(',',''))
				except:
					central_bid = 0
				try:
					percent_strike = central_bid/central_strikes
				except:
					percent_strike = -1

				while True:
					try:
						sql.execute(f"INSERT INTO central_strikes VALUES(?,?,?,?,?,?,?)",(tickets[pos],central_strikes,central_bid,percent_strike,date_now,'puts',str(datetime.datetime.date(dateparser.parse(date_future)))))
						db.commit()
						break
					except:
						print('Load1 to base')
						sleep(0.5)


				for row in range(len(strikes)):
					insert_strike = float(strikes[row].next_element.get_text().replace(',',''))
					try:
						insert_bid = float(price_bid[row].get_text().replace(',',''))
					except:
						insert_bid = -1
					try:
						insert_ask = float(price_ask[row].get_text().replace(',',''))
					except:
						insert_ask = -1
					try:
						insert_volume = int(volumes[row].get_text())
					except:
						insert_volume = -1
					try:
						insert_interest = int(open_interest[row].get_text().replace(',',''))
					except:
						insert_interest = -1
					while True:
						try:
							sql.execute(f"INSERT INTO puts_future VALUES(?,?,?,?,?,?,?,?,?,?)",(tickets[pos],insert_strike,insert_bid,insert_ask,insert_volume,insert_interest,str(datetime.datetime.date(dateparser.parse(date_future))),date_now,type_stock,type_time))
							db.commit()
							break
						except:
							print('Load2 to base')
							sleep(0.5)
				while True:
					try:
						self.drivers[num_drivers].find_element_by_xpath('//select[@class="Fz(s) H(25px) Bd Bdc($seperatorColor)"]').click()
						break
					except:
						print('load3 to base')
						sleep(0.5)
				while True:
					try:
						all_options = self.drivers[num_drivers].find_elements_by_xpath('//option')
						break
					except:
						print('load4 to base')
						sleep(0.5)


	def quit(self):
		for drive in self.drivers:
			print('выходи')
			drive.quit()





	def parsing_thread(self,tickets,num_drivers,count_tickets):
		position_before = int((count_tickets/NUM_THREADS) * num_drivers)
		position_after = int((count_tickets/NUM_THREADS) * (num_drivers+1))
		for pos in range(position_before,position_after):
			self.parsing_one(pos,tickets,num_drivers)
		global mask_threads
		mask_threads[num_drivers] = 1
		print('Vse')



	def parsing_all(self):
		file = open('tickets.txt','r')
		tickets = []
		for line in file:
			tickets.append(line)
		file.close()
		count_tickets = len(tickets)
		for i in range(NUM_THREADS):
			threading.Thread(target=self.parsing_thread,args=[tickets,i,count_tickets]).start()
		print('Exit')


class DB:
	def __init__(self):
		self.db = sqlite3.connect('info.db')
		self.sql = self.db.cursor()

	def delete_all_today(self):
		today = str(datetime.datetime.date(datetime.datetime.now()))
		while True:
			try:
				self.sql.execute(f"DELETE FROM calls_now WHERE date_add = '" + today +"'")
				self.db.commit()
				break
			except:
				sleep(2)
		while True:
			try:
				self.sql.execute(f"DELETE FROM puts_now WHERE date_add = '" + today +"'")
				self.db.commit()
				break
			except:
				sleep(2)
		while True:
			try:
				self.sql.execute(f"DELETE FROM puts_future WHERE date_add = '" + today +"'")
				self.db.commit()
				break
			except:
				sleep(2)
		while True:
			try:
				self.sql.execute(f"DELETE FROM prices")
				self.db.commit()
				break
			except:
				sleep(2)

	def get_diaposon(self,array):
		minn = 10000000
		maxx = -1000
		value = 0
		for row in array:
			if row[0]> maxx:
				maxx = row[0]
			if row[0]<minn:
				minn = row[0]
			value+=row[0]
		return (minn,maxx),value/len(array)

	def sort_of_strike(self,strikes):
		for i in range(len(strikes)):
			for j in range(i+1,len(strikes)):
				if strikes[j][3] > strikes[i][3]:
					strikes[j],strikes[i] = strikes[i],strikes[j]
		return strikes

	def write_csv(self,file_name,first_row,other_rows):
		with open(file_name, "w", newline="") as file:
		    writer = csv.writer(file,delimiter=';')
		    writer.writerow(first_row)
		    writer.writerows(other_rows)

	def analis_result3(self,results):
		for i in range(len(results)):
			for j in range(i+1,len(results)):
				if results[i][1] * results[i][3] < results[j][1]*results[j][3]:
					results[i],results[j] = results[j],results[i]
		return results


	def result_3(self,dirs):
		file = open('tickets.txt','r')
		tickets = []
		for line in file:
			tickets.append(line)
		file.close()

		rows_result = []
		for row in tickets:
			self.sql.execute(f'SELECT strike,price_bid FROM calls_now WHERE ticker = ? AND date_add = ?',(row,str(datetime.datetime.date(datetime.datetime.now()))))
			results = self.sql.fetchall()
			self.sql.execute(f"SELECT price FROM prices WHERE ticker = ?",(row,))
			price_stoke = self.sql.fetchall()
			for res in results:
				if res[0]<=0.95 * price_stoke[0][0] and res[0]>=0 and res[1]>=0:
					percent_left = 100 - (res[0] * 100)/price_stoke[0][0]
					feet = (res[0] + res[1] - price_stoke[0][0]) * 100
					percent_feet = (feet * 100) / price_stoke[0][0]
					rows_result.append([row,percent_left,res[0],percent_feet])
		final_results = self.analis_result3(rows_result)
		self.write_csv(dirs + '\\result3.csv',['Акция','Цена ниже на,%','Страйк','Процент заработка'],final_results)


	def result_1_2(self,dirs):
		file = open('tickets.txt','r')
		tickets = []
		for line in file:
			tickets.append(line)
		file.close()
		self.sql.execute(f"SELECT ticker,central_strike,bid,percent FROM central_strikes WHERE date = ? AND type = ?",(str(datetime.datetime.date(datetime.datetime.now())),'calls'))
		results = self.sql.fetchall()
		strikes = self.sort_of_strike(results)

		new_strikes = []
 
		for row in tickets:
			self.sql.execute(f"SELECT percent FROM central_strikes WHERE ticker = ? AND type = ?",(row,'calls'))
			results = self.sql.fetchall()
			diaposon,average = self.get_diaposon(results)
			for strike in strikes:
				if strike[0] == row:
					strike[-1]*=100
					new_strikes.append(list(strike) + ['['+str(diaposon[0]) + ':'+ str(diaposon[1]) + ']'])


		self.write_csv(dirs + '\\result_1_2.csv',['Тикер','Центральный страйк','Бид','Процент','Диапазон'],new_strikes)

	def result_4(self,dirs):
		file = open('tickets.txt','r')
		tickets = []
		for line in file:
			tickets.append(line)
		file.close()

		puts_info = []


		for row in tickets:
			self.sql.execute(f"SELECT bid,percent FROM central_strikes WHERE ticker = ? AND type = ? AND date = ?",(row,'calls',str(datetime.datetime.date(datetime.datetime.now()))))
			list_info = self.sql.fetchall()
			call_bid,percent_bid = list_info[0][0],list_info[0][1]
			self.sql.execute(f"SELECT central_strike,bid,date_option FROM central_strikes WHERE ticker = ? AND type = ?",(row,'puts'))
			results = self.sql.fetchall()
			for res in results:
				week_pay = ceil(res[1]/call_bid)
				date_option = datetime.datetime.strptime(res[2],'%Y-%m-%d')
				week_free = deest_week(datetime.datetime.now(),date_option)
				percent_week = week_pay/week_free
				final_feet = percent_bid - percent_bid * percent_week
				puts_info.append([row,date_option,week_free,float(res[0]),percent_bid*100,percent_week*100,final_feet*100])

		self.write_csv(dirs + '\\result4.csv',['Акция','Дата','Количество недель','Центральный страйк','Изначальный процент','Процент неокупаемых недель','Прибыль'],puts_info)
		self.analis_result4(puts_info,dirs)

	def analis_result4(self,results,dirs):
		for i in range(len(results)):
			for j in range(i+1,len(results)):
				if results[i][6]/results[i][2] < results[j][6]/results[j][2]:
					results[i],results[j] = results[j],results[i]
		new_results = []
		for i in range(int(0.1*len(results))):
			new_results.append(results[i])

		self.write_csv(dirs + '\\result4_feets.csv',['Акция','Дата','Количество недель','Центральный страйк','Изначальный процент','Процент неокупаемых недель','Прибыль'],new_results)

	def delete_prev(self):
		today = datetime.datetime.date(datetime.datetime.now())
		while True:
			try:
				self.sql.execute(f"DELETE FROM central_strikes")
				self.db.commit()
				break
			except:
				sleep(2)
		while True:
			try:
				self.sql.execute(f"DELETE FROM prices")
				self.db.commit()
				break
			except:
				sleep(2)

		while True:
			try:
				self.sql.execute(f"DELETE FROM calls_now")
				self.db.commit()
				break
			except:
				sleep(2)

		while True:
			try:
				self.sql.execute(f"DELETE FROM puts_future")
				self.db.commit()
				break
			except:
				sleep(2)

		while True:
			try:
				self.sql.execute(f"DELETE FROM puts_now")
				self.db.commit()
				break
			except:
				sleep(2)

def deest_week(d1,d2):
	monday1 = (d1 - datetime.timedelta(days=d1.weekday()))
	monday2 = (d2 - datetime.timedelta(days=d2.weekday()))
	return int((monday2 - monday1).days / 7)	

def create_mdir():
	name = str(datetime.datetime.now()).replace(' ','_').replace('.','_').replace(':','_')
	os.mkdir(name)
	dirs = os.path.abspath(os.curdir) + '\\' + name
	return dirs

def main_func():
	dbs = DB()
	dbs.delete_prev()
	global mask_threads
	while True:
		dbs.delete_prev()
		bot = Bot()
		times = datetime.datetime.now().astimezone(pytz.timezone('America/New_York')).time()
		if times>= datetime.time(hour=9,minute = 40) and times<=datetime.time(hour=16) and datetime.datetime.now().astimezone(pytz.timezone('America/New_York')).weekday() != 5 and datetime.datetime.now().astimezone(pytz.timezone('America/New_York')).weekday() != 6 :
			bot.parsing_all()
			now = datetime.datetime.now()
			while mask_threads!=[1,1,1,1,1]:
				if now - datetime.datetime.now() >=datetime.timedelta(hours=3):
					break
				print('mask_threads',mask_threads)
				sleep(10)
			bot.quit()
			dirs = create_mdir()
			dbs.result_4(dirs)
			if datetime.datetime.now().astimezone(pytz.timezone('America/New_York')).weekday() == 0:
				dbs.result_3(dirs)
				dbs.result_1_2(dirs)
			mask_threads = [0,0,0,0,0]
		sleep(1000)



if __name__ == '__main__':
	print('Запуск...')
	main_func()
	


